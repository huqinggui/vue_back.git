import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/a',
    name: 'A',
    component: () => import(/* webpackChunkName: "about" */ '../views/A.vue')
  },
  {
    path: '/b',
    name: 'B',
    component: () => import(/* webpackChunkName: "about" */ '../views/B.vue')
  },
  {
    path: '/c',
    name: 'C',
    component: () => import(/* webpackChunkName: "about" */ '../views/C.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
